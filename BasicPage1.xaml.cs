﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Magic_player
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class BasicPage1 : Magic_player.Common.LayoutAwarePage
    {
       private PhotoCapture photocapt;
        public BasicPage1()
        {
            this.InitializeComponent();
             photocapt = new PhotoCapture();
        }

     
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

       
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void Photo_Click(object sender, RoutedEventArgs e)
        {
            photocapt.CameraCapture();
            
        }

        private void ShowPhoto_Click(object sender, RoutedEventArgs e)
        {
            photo.Source = photocapt.wBitmap;
        }

        private void SavePicture_Click(object sender, RoutedEventArgs e)
        {
            photocapt.SaveImageAsJpeg();
        }
    }
}
