﻿using System;
using System.Linq;
using magic_player.Interfaces;
using Windows.Storage.FileProperties;

namespace magic_player
{
    public class AudioTrack : IAudioTrack
    {
        public string Title { get { return Properties.Title; } }
        public string Artist { get { return Properties.Artist; } }
        public string Duration { get { return Properties.Duration.ToString(@"mm\:ss"); } }
        public string Genre { get { return string.Join(" ", Properties.Genre.ToArray()); } }
        public string Rating { get { return Properties.Rating.ToString(); } }
        public string Year { get { return Properties.Year.ToString(); } }

        public MusicProperties Properties { get; set; }
        public Uri ThumbUri { get; set; }

        public static IAudioTrack Empty = new EmptyAudioTrack();
    }

    public class EmptyAudioTrack : IAudioTrack
    {
        public string Title { get { return "No audio file selected"; } }
        public string Artist { get { return "Please use the Application Bar below by swiping from the bottom edge of the screen or right-clicking"; } }
        public string Duration { get { return string.Empty; } }
        public string Genre { get { return string.Empty; } }
        public string Rating { get { return string.Empty; } }
        public string Year { get { return string.Empty; } }
        public Uri ThumbUri { get { return new Uri("ms-appx:///Assets/mp3_player.png");} set { }}
    }
}