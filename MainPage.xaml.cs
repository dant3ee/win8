﻿using System.Threading.Tasks;
using magic_player.Interfaces;
using magic_player.ViewModels;
using Windows.Media;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Notifications;
using System;
using Windows.Data.Xml.Dom;
using Windows.Storage.Streams;



namespace magic_player
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : IFileSource
    {
        private readonly MainViewModel _viewModel;
        private DispatcherTimer _timer;
        private SystemMediaTransportControls systemControls;
        public MainPage()
        {
           
            _viewModel = new MainViewModel(this);
            
            DataContext = _viewModel;
            InitializeComponent();
            PlayButton.Visibility = Visibility.Collapsed;
            Video.Visibility = Visibility.Collapsed;

            // Hook up app to system transport controls.
            systemControls = SystemMediaTransportControls.GetForCurrentView();
            systemControls.ButtonPressed += SystemControls_ButtonPressed;

            // Register to handle the following system transpot control buttons.
            systemControls.IsPlayEnabled = true;
            systemControls.IsPauseEnabled = true;

            
        }


        void SystemControls_ButtonPressed(SystemMediaTransportControls sender,
    SystemMediaTransportControlsButtonPressedEventArgs args)
        {
            switch (args.Button)
            {
                case SystemMediaTransportControlsButton.Play:
                    PlayMedia();
                    break;
                case SystemMediaTransportControlsButton.Pause:
                    PauseMedia();
                    break;
                default:
                    break;
            }
        }

        async void PlayMedia()
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                myMedia.Play();
            });
        }

        async void PauseMedia()
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                myMedia.Pause();
            });
        }
        void myMedia_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            switch (myMedia.CurrentState)
            {
                case MediaElementState.Playing:
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Playing;
                    break;
                case MediaElementState.Paused:
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Paused;
                    break;
                case MediaElementState.Stopped:
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Stopped;
                    break;
                case MediaElementState.Closed:
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Closed;
                    break;
                default:
                    break;
            }
        }


        public async Task<StorageFile> ReadAudioFile()
        {
            var fileOpenPicker = new FileOpenPicker();
            fileOpenPicker.FileTypeFilter.Add(".wmv");
            fileOpenPicker.FileTypeFilter.Add(".mp4");
            fileOpenPicker.FileTypeFilter.Add(".mp3");
            fileOpenPicker.FileTypeFilter.Add(".wma");

            fileOpenPicker.SuggestedStartLocation = PickerLocationId.MusicLibrary;
            StorageFile file = await fileOpenPicker.PickSingleFileAsync();
            if (file == null)
                return null;

            var stream = await file.OpenAsync(FileAccessMode.Read);
            if (file.FileType == ".mp4") {
                myMedia.Stop();
                currentTime.Visibility = Visibility.Collapsed;
                PlayButton.Visibility = Visibility.Collapsed;
                myMedia.Visibility = Visibility.Collapsed;
                img.Visibility = Visibility.Collapsed;
                Video.SetSource(stream, file.ContentType);
                Video.Visibility = Visibility.Visible;
            }else
            {
                Video.Stop();
                currentTime.Visibility = Visibility.Visible;
                img.Visibility = Visibility.Visible;
                Video.Visibility = Visibility.Collapsed;
                myMedia.SetSource(stream, file.ContentType);
                myMedia.Visibility = Visibility.Visible;
                PlayButton.Visibility = Visibility.Visible;
                PlayButton.Style = (Style)Application.Current.Resources["PauseAppBarButtonStyle"];
                
            }
          
            return file;
        }



        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void MyMediaMediaFailed1(object sender, ExceptionRoutedEventArgs e)
        {
            if (e != null && !string.IsNullOrEmpty(e.ErrorMessage))
            {
                var messageDialog = new MessageDialog(e.ErrorMessage);
                messageDialog.ShowAsync();
            }
        }

        private void MyMediaMediaOpened1(object sender, RoutedEventArgs e)
        {
            
            SetupTimer();
        }

        private void SetupTimer()
        {
            _timer = new DispatcherTimer {Interval = TimeSpan.FromMilliseconds(300)};
            StartTimer();
        }

        private void TimerTick(object sender, object e)
        {
            currentTime.Text = myMedia.Position.ToString(@"mm\:ss");
        }

        private void StartTimer()
        {
            _timer.Tick += TimerTick;
            _timer.Start();
        }

        private void StopTimer()
        {
            _timer.Stop();
            _timer.Tick -= TimerTick;
        }

        private void PlayButtonClick(object sender, RoutedEventArgs e)
        {
            
            if (myMedia.CurrentState == MediaElementState.Playing)
            {
                myMedia.Pause();
                PlayButton.Style = (Style)Application.Current.Resources["PlayAppBarButtonStyle"];
                
            }
            else
            {
                myMedia.Play();
                PlayButton.Style = (Style)Application.Current.Resources["PauseAppBarButtonStyle"];
                
            }
        }

        public Task<StorageFile> GetFileSource()
        {
            return ReadAudioFile();
        }

        private void newSoundtrack() {
            var notificationXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText01);
            var toastElements = notificationXml.GetElementsByTagName("text");
            toastElements[0].AppendChild(notificationXml.CreateTextNode("" + _viewModel.AudioTrack.Artist + " - " + _viewModel.AudioTrack.Title + " " + _viewModel.AudioTrack.Duration));
            var toastNotification = new ToastNotification(notificationXml);
            ToastNotificationManager.CreateToastNotifier().Show(toastNotification);

        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var notificationXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText01);
            var toastElements = notificationXml.GetElementsByTagName("text");
            toastElements[0].AppendChild(notificationXml.CreateTextNode(""+_viewModel.AudioTrack.Artist+" - "+_viewModel.AudioTrack.Title+" "+_viewModel.AudioTrack.Duration));
            var toastNotification = new ToastNotification(notificationXml); 
            ToastNotificationManager.CreateToastNotifier().Show(toastNotification);

        }

 
      


        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Magic_player.BasicPage1));
        }




    }
}
