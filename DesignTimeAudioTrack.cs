﻿using System;
using magic_player.Interfaces;

namespace magic_player
{
    public class DesignTimeAudioTrack : IAudioTrack
    {
        public string Title { get { return "XXX XXXXXXXXXXXX"; } }
        public string Artist { get { return "XXX XXXXX XXXXXXXXXXXXXXXX"; } }
        public string Duration { get { return "00:46"; } }
        public string Genre { get { return "Pop"; } }
        public string Rating { get { return "5"; } }
        public string Year { get { return "1969"; } }
        public Uri ThumbUri { get { return new Uri("ms-appx:///assets/logo.png"); }
            set {}
        }
    }
}