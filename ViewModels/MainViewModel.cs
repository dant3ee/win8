﻿using System;
using magic_player.Interfaces;
using magic_player.Mvvm;
using Windows.Media;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Notifications;
namespace magic_player.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IFileSource _fileSource;

        public MainViewModel()
        {
            if (IsDesignMode)
            {
                AudioTrack = new DesignTimeAudioTrack();
            }
        }

        public MainViewModel(IFileSource fileSource)
        {
            _fileSource = fileSource;
            OpenCommand = new DelegateCommand(ExecuteOpen);

            AudioTrack = magic_player.AudioTrack.Empty;
            
        }

        private async void ExecuteOpen(object obj)
        {
            StorageFile storageFile = await _fileSource.GetFileSource();
            if (storageFile != null)
                SetFile(storageFile);
           
        }


        private IAudioTrack _audioTrack;

        public IAudioTrack AudioTrack
        {
            get { return _audioTrack; }
            set
            {
                if (_audioTrack != value)
                {
                    _audioTrack = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private void newSoundtrack()
        {
            var notificationXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText01);
            var toastElements = notificationXml.GetElementsByTagName("text");
            toastElements[0].AppendChild(notificationXml.CreateTextNode("" + AudioTrack.Artist + " - " + AudioTrack.Title + " " + AudioTrack.Duration));
            var toastNotification = new ToastNotification(notificationXml);
            ToastNotificationManager.CreateToastNotifier().Show(toastNotification);

        }
        public DelegateCommand OpenCommand { get; set; }

        public async void SetFile(StorageFile file)
        {
            var audioTrack = new AudioTrack { Properties = await file.Properties.GetMusicPropertiesAsync() };
            string filename = audioTrack.Properties.Title + ".png";
            audioTrack.ThumbUri = new Uri("ms-appdata:///local/" + filename);

            // Store the file thumbnail in local applicatin storage
            using (StorageItemThumbnail storageItemThumbnail = await file.GetThumbnailAsync(ThumbnailMode.SingleItem, 500, ThumbnailOptions.ResizeThumbnail))
            using (IInputStream inputStreamAt = storageItemThumbnail.GetInputStreamAt(0))
            using (var dataReader = new DataReader(inputStreamAt))
            {
                uint u = await dataReader.LoadAsync((uint)storageItemThumbnail.Size);
                IBuffer readBuffer = dataReader.ReadBuffer(u);

                var tempFolder = ApplicationData.Current.LocalFolder;
                var imageFile = await tempFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

                using (IRandomAccessStream randomAccessStream = await imageFile.OpenAsync(FileAccessMode.ReadWrite))
                using (IOutputStream outputStreamAt = randomAccessStream.GetOutputStreamAt(0))
                {
                    await outputStreamAt.WriteAsync(readBuffer);
                    await outputStreamAt.FlushAsync();
                }
            }

            // Finally, set up the Media Control
            MediaControl.TrackName = audioTrack.Properties.Title;
            MediaControl.ArtistName = audioTrack.Properties.Artist;
            MediaControl.AlbumArt = audioTrack.ThumbUri;

            AudioTrack = audioTrack;
            newSoundtrack();
        }
    }
}