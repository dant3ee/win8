using System;

namespace magic_player.Interfaces
{
    public interface IAudioTrack
    {
        string Title { get; }
        string Artist { get; }
        string Duration { get; }
        string Genre { get; }
        string Rating { get; }
        string Year { get; }

        Uri ThumbUri { get; set; }
    }
}