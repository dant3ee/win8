using System.Threading.Tasks;
using Windows.Storage;

namespace magic_player.Interfaces
{
    public interface IFileSource
    {
        Task<StorageFile> GetFileSource();
    }
}